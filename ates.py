#!/usr/bin/env python
# encoding: utf-8

import sys

def solve(value, atomlist, eq=""):
    mincomp = -10
    maxcomp = 15
    for i in range(mincomp, maxcomp):
        rvalue = value + i * atomlist[0][1]
        if len(atomlist) > 1:
            req = eq+str(i)+"*"+str(atomlist[0][0])+"+"
            solve(rvalue, atomlist[1:], req)
        else:
            req = eq+str(i)+"*"+str(atomlist[0][0])
            if round(rvalue, 5) == 0:
                print req
    


if __name__ ==  "__main__":
    if len(sys.argv) == 2:
        solve(-float(sys.argv[1]), [("C", 12.), ("H", 1.00782503207), 
        ("O", 15.99491461956), ("N", 14.0030740048), 
        ("S", 31.97207100), ("Se", 79.9165213)])
    else:
        print "no"


